all:		geometrica.o retangulo.o main.o
		g++ -o main geometrica.o retangulo.o main.o

geometrica.o:	geometrica.cpp geometrica.hpp
		g++ -c geometrica.cpp

retangulo.o:	retangulo.cpp retangulo.hpp
		g++ -c retangulo.cpp

main.o:		main.cpp retangulo.hpp
		g++ -c main.cpp

clean:
		rm -rf *.o

run:
		./main
